/*
  Simple HTTP library

  for later reference http://www.jmarshall.com/easy/http/
 */

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

/* #include <fcntl.h> /\* only for the webm *\/ */
/* #include <sys/sendfile.h> */

#include "http.h"


#define HTTP_DEFAULT_PORT "8080"
/* #define BUF_SIZE 1500 */
#define BACKLOG_SIZE 5
/* #define TESTGET "GET /" */
/* #define HTTP1HEADER "HTTP/1.0 200 OK\n\r" */



char
httpAddHander(httpServer *server,handler function) {
    handler *new;
    new = realloc(server, server->handlersLen);
    if (new == NULL) {
        return -1;
    }
    server->handlers = new;
    server->handlers[server->handlersLen] = function;
    server->handlersLen++;

    return 0;
}

void
httpDeleteServer(httpServer *server) {
    close(server->sfd);
    free(server->handlers);
    free(server);
}

httpServer *
httpCreateServer(const char * const restrict port) {
    httpServer *server;
    struct addrinfo hints, *results, *rp;
    int addrinfoRet;

    server = calloc(sizeof(httpServer), 1);

    if (server == NULL) {
        return NULL;
    }

    memset(&hints, '\0', sizeof(struct sockaddr_in));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    hints.ai_protocol = 0;
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    addrinfoRet = getaddrinfo(NULL, port, &hints, &results);
    if (addrinfoRet != 0){
        /* fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(addrinfoRet)); */
        free(server);
        return NULL;
    }

    for (rp = results; rp != NULL; rp = rp->ai_next) {
        /* If error go to the next one */
        if ((server->sfd = socket(rp->ai_family, rp->ai_socktype,
                          rp->ai_protocol)) == -1)
            continue;

        if (bind(server->sfd, rp->ai_addr, rp->ai_addrlen) == 0)
            break;

        close(server->sfd);
    }

    if (rp == NULL) { /* No address succeeded */
        /* fputs("Could not bind\n", stderr); */
        /* fputs("The port might be in use, " */
              /* "or you might need root privilages for that port\n", stderr); */
        httpDeleteServer(server);
        return NULL;
    }

    freeaddrinfo(results); /* No longer needed */

    return server;
}

char
httpStartListening(httpServer *server) {
        if (listen(server->sfd, BACKLOG_SIZE) == -1) {
            /* Failed to start listening */
            return -1;
        }

        return 0;
}

/* static void * */
/* httpGetAddr(void *addr) { */

/*     if (((struct sockaddr *)addr)->sa_family == AF_INET) */
/*         return &(((struct sockaddr_in *)addr)->sin_addr); */
/*     else */
/*         return &(((struct sockaddr_in6 *)addr)->sin6_addr); */

/* } */


/* Add a function that handles multiple server */
char
httpHandle(httpServer *server) {
    struct sockaddr_storage peer_addr;
    socklen_t peer_addr_len;
    int peerfd;
    int i;
    /* char handlerRet; */

    peer_addr_len = sizeof(struct sockaddr_storage);

    peerfd = accept(server->sfd, (struct sockaddr *) &peer_addr,
                    &peer_addr_len);

    if (peerfd == -1) {
        return -1;
    }

    for (i = 0;i < server->handlersLen; i++) {
        /* Should the loop continue even if a handler returns non-zero value? */
        /* So it could work like a switch/case or allow user to add backup handlers */
        server->handlers[i](peerfd);
    }
    /* Might have forgotten something */

    close(peerfd);
    return 0;
}


/* Things left to add for now */

/*     int sfd, s; */
/*     struct addrinfo hints; */
/*     struct addrinfo *results, *rp; */
/*     struct sockaddr_storage peer_addr; /\* Connectors address information *\/ */
/*     socklen_t peer_addr_len; */

/*         char address[INET6_ADDRSTRLEN]; */
/*         int peerfd; */
/*         char buffer[BUF_SIZE] = {0}; */
/*         ssize_t nread; */
/*         peer_addr_len = sizeof(struct sockaddr_storage); */


/*         nread = recv(peerfd, buffer, BUF_SIZE, 0); */
/*         if (nread == -1) { */
/*             perror("Could not recieve a message"); */
/*             goto cont; */
/*         } else if (nread == 0) { */
/*             printf("Socket to %s has been shutdown\n", address); */
/*             goto cont; */
/*         } */

/*         if (strncmp(buffer, TESTGET, strlen(TESTGET)) == 0) { */

/*             int tmp = open("uwu.webm", O_RDONLY); /\* Who needs error checking btw *\/ */
/*             sendfile(peerfd, tmp, 0, 4193123); */
/*             close(tmp); */

/*             /\* if (send(peerfd, msg, strlen(msg), 0) == -1) { *\/ */
/*             /\*     fprintf(stderr, "Error while sending a message to %s: %s", *\/ */
/*             /\*             address, strerror(errno)); *\/ */
/*             /\*     goto cont; *\/ */
/*             /\* } *\/ */
/*         } */
/*     cont: */
/*         close(peerfd); */
/*     } */
/*     close(sfd); */

/*     return 0; */
/* } */
















/* whole, working program that sends a given file */

/* int */
/* main() { */
/*     int sfd, s; */
/*     struct addrinfo hints; */
/*     struct addrinfo *results, *rp; */
/*     struct sockaddr_storage peer_addr; /\* Connectors address information *\/ */
/*     socklen_t peer_addr_len; */


/*     /\* ssize_t nread; *\/ */
/*     /\* char buf[BUF_SIZE]; *\/ */

/*     memset(&hints, '\0', sizeof(struct sockaddr_in)); */
/*     hints.ai_family = AF_UNSPEC; */
/*     hints.ai_socktype = SOCK_STREAM; */
/*     hints.ai_flags = AI_PASSIVE; */
/*     hints.ai_protocol = 0; */
/*     hints.ai_canonname = NULL; */
/*     hints.ai_addr = NULL; */
/*     hints.ai_next = NULL; */

/*     s = getaddrinfo(NULL, DEFAULT_PORT, &hints, &results); */
/*     if (s != 0){ */
/*         fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s)); */
/*         exit(EXIT_FAILURE); */
/*     } */

/*     for (rp = results; rp != NULL; rp = rp->ai_next) { */
/*         /\* If error go to the next one *\/ */
/*         if ((sfd = socket(rp->ai_family, rp->ai_socktype, */
/*                           rp->ai_protocol)) == -1) */
/*             continue; */

/*         if (bind(sfd, rp->ai_addr, rp->ai_addrlen) == 0) */
/*             break; */

/*         close(sfd); */
/*     } */

/*     if (rp == NULL) { /\* No address succeeded *\/ */
/*         fputs("Could not bind\n", stderr); */
/*         fputs("The port might be in use, " */
/*               "or you might need root privilages for that port\n", stderr); */
/*         close(sfd); */
/*         exit(EXIT_FAILURE); */
/*     } */

/*     freeaddrinfo(results); /\* No longer needed *\/ */

/*     /\* listen *\/ */

/*     if (listen(sfd, BACKLOG_SIZE) == -1) { */
/*         perror("Failed to start listening"); */
/*         close(sfd); */
/*         exit(EXIT_FAILURE); */
/*     } */

/*     puts("Waiting for connections…\n"); */

/*     /\* Read messages and send them back, temporary *\/ */

/*     for (;;) { */
/*         /\* char msg[] = "Hello 世界!\n"; *\/ */
/*         char address[INET6_ADDRSTRLEN]; */
/*         int peerfd; */
/*         char buffer[BUF_SIZE] = {0}; */
/*         ssize_t nread; */
/*         peer_addr_len = sizeof(struct sockaddr_storage); */

/*         /\* accept *\/ */

/*         peerfd = accept(sfd, (struct sockaddr *) &peer_addr, &peer_addr_len); */

/*         if (peerfd == -1) { */
/*             perror("Could not accept peer"); */
/*             goto cont; */
/*         } */

/*         inet_ntop(peer_addr.ss_family, httpGetAddr(&peer_addr), */
/*                   address, sizeof(address)); */
/*         printf("Got a connection from %s.\n", address); */

/*         /\* send/recieve *\/ */

/*         nread = recv(peerfd, buffer, BUF_SIZE, 0); */
/*         if (nread == -1) { */
/*             perror("Could not recieve a message"); */
/*             goto cont; */
/*         } else if (nread == 0) { */
/*             printf("Socket to %s has been shutdown\n", address); */
/*             goto cont; */
/*         } */

/*         if (strncmp(buffer, TESTGET, strlen(TESTGET)) == 0) { */

/*             int tmp = open("uwu.webm", O_RDONLY); /\* Who needs error checking btw *\/ */
/*             sendfile(peerfd, tmp, 0, 4193123); */
/*             close(tmp); */

/*             /\* if (send(peerfd, msg, strlen(msg), 0) == -1) { *\/ */
/*             /\*     fprintf(stderr, "Error while sending a message to %s: %s", *\/ */
/*             /\*             address, strerror(errno)); *\/ */
/*             /\*     goto cont; *\/ */
/*             /\* } *\/ */
/*         } */
/*     cont: */
/*         close(peerfd); */
/*     } */
/*     close(sfd); */

/*     return 0; */
/* } */

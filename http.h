typedef char (*handler)(int peerfd);



typedef struct httpServer {
    int sfd;
    handler *handlers;
    unsigned short handlersLen;
} httpServer;



char httpAddHander(httpServer *server,handler function);
void httpDeleteServer(httpServer *server);
httpServer *httpCreateServer(const char * const restrict port);
char httpStartListening(httpServer *server);
char httpHandle(httpServer *server);
